#!/bin/bash

rm -rf headers
mkdir headers

pushd headers
	KPREFIX=/lib/modules/$(uname -r)/build
	ln -s /lib/modules/$(uname -r)/build/include/asm-generic asm
	gcc -nostdlib -c -fdump-ada-spec -C ${KPREFIX}/include/linux/kernel.h -I ${KPREFIX}/include -I ${KPREFIX}/include/linux/atomic -I ${KPREFIX}/arch/x86/include/ -I .
	gcc -nostdlib -c -fdump-ada-spec -C ${KPREFIX}/include/linux/module.h -I ${KPREFIX}/include -I ${KPREFIX}/include/linux/atomic -I ${KPREFIX}/arch/x86/include/ -I .
	rm asm
popd
