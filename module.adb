with Interfaces.C;

package SomeModule is

  -- Module Variables

  type Aliased_String is array (Positive range <>)
  of aliased Character;
  pragma Convention (C, Aliased_String);

  Kernel_Version: constant Aliased_String:="2.4.18-5" & Character'Val(0);
  pragma Export (C, Kernel_Version, "kernel_version");

  Mod_Use_Count: Integer;
  Pragma Export (C, Mod_Use_Count, "mod_use_count_");

  -- Kernel Calls

  procedure Printk( s : string );
  pragma import( C, printk, "printk" );

  -- Our Module Functions

  function Init_Module return Interfaces.C.int;
  pragma Export (C, Init_Module, "init_module");

  procedure Cleanup_Module;
  pragma Export (C, Cleanup_Module, "cleanup_module");
end SomeModule;

package body SomeModule is
  -- sample module layout

  function Init_Module return Interfaces.C.int is
  begin
    Printk("Hello,World!" & Character'val(10) & character'val(0));
    return 0;
  end Init_Module;

  procedure Cleanup_Module is
  begin
    Printk("Goodbye , World!" & Character'val(10) & character'val(0));
  end Cleanup_Module;
 end SomeModule;
